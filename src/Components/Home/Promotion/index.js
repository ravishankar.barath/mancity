import React, { Component } from 'react';
import PromotionAnimation from './Animation.js'
import Enroll from './Enroll';

class Promotion extends Component {
    render() {
        return (
           <div className="promotion_wrapper" style={{background:'#ffffff'}}>
               <div className="container">
                   <PromotionAnimation/>
                   <Enroll/>
               </div>

           </div>
        );
    }
}

export default Promotion;