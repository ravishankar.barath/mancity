import React, { Component } from 'react';
import Stripes from '../../../Resources/Resources/images/stripes.png'
import {Tag} from '../../UI/misc';
import Reveal from 'react-reveal/Reveal';
import HomeCards from './cards.js';

class MeetPlayers extends Component {
    
    state={
        show:false

    }
    render() {
        console.log("showstate",this.state.show);
        return (
            <Reveal
                fraction={0.7}
                onReveal={()=>(
                   this.setState(
                       {show:true}
                       )
                )}
            >
                <div className="home_meetplayers"
                style={{
                     background:`#ffffff url(${Stripes})`
                }}
            >
                <div className="container">
                    <div className="home_meetplayers_wrapper">
                        <div className="home_card_wrapper">
                            <HomeCards
                                show={this.state.show}
                                
                            />

                    
                        </div>
                        <div className="home_text_wrapper">
                            <Tag 
                                bck="#0e1731"
                                size="100px"
                                color="#ffffff"
                                mb='20px'
                                add={{
                                    display:'inline-block',
                                    
                                }}
                            >
                                Meet


                            </Tag>
                            <Tag 
                                bck="#0e1731"
                                size="100px"
                                color="#ffffff"
                                mb="20px"
                                add={{
                                    display:'inline-block',
                                   
                                }}
                            >
                                The


                            </Tag>
                            <Tag 
                                bck="#0e1731"
                                size="100px"
                                color="#ffffff"
                                mb='20px'
                                add={{
                                    display:'inline-block',
                                    
                                }}
                            >
                                Players


                            </Tag>
                        </div>
                        <Tag 
                        bck="#ffffff"
                        size="27px"
                        color="#0e1731"
                        link={true}
                        linkto="/the_team"
                        mb='27px'
                        add={{
                            display:'inline-block',
                            
                            border:'1px solid #0e1731'
                        }}

                    >
                        Meet The Players

                    </Tag>

                    </div>
                    
                </div>
            </div>
            </Reveal>
            
        );
    }
}

export default MeetPlayers;