import React from 'react';
import Featured from './featured/index';
import Matches from './Matches/index';
import MeetPlayers from './MeetPlayers/index';
import Promotion from './Promotion/index'
const index = () => {
    return (
        <div className="bck_blue">
            <Featured/>
            <Matches/>
            <MeetPlayers/>
            <Promotion/>

        </div>
    );
};

export default index;