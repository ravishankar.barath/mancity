import firebase from 'firebase/app';
import 'firebase/app';
import 'firebase/database';

const config = {
    apiKey: "AIzaSyDfyCnwQXkAnZHw6CL7J56vhlgnVatvmbw",
    authDomain: "mancitym.firebaseapp.com",
    databaseURL: "https://mancitym.firebaseio.com",
    projectId: "mancitym",
    storageBucket: "mancitym.appspot.com",
    messagingSenderId: "519810009284",
    appId: "1:519810009284:web:70df4b0a64e8b0dcffe191",
    measurementId: "G-YJGD2B4Y8K"
};

firebase.initializeApp(config);

const firebaseDB = firebase.database();
const firebaseMatches=firebaseDB.ref('matches');
const firebasePromotions = firebaseDB.ref('promotions');


export{
    firebase,
    firebaseMatches,
    firebasePromotions
}